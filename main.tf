terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "~> 3.0.2"
    }
  }
  required_version = ">= 1.1.0"
}
provider "azurerm"{
  features{}
}

resource "azurerm_resource_group" "rg" {
  name = "rg-${var.project_name}-${var.location}"
  location = var.location
}
resource "azurerm_virtual_network" "vnet" {
  name = "vn-${var.project_name}-${var.location}" 
  location            = azurerm_resource_group.rg.location
  resource_group_name = azurerm_resource_group.rg.name
  address_space       = ["10.0.0.0/16"]
  dns_servers         = ["168.63.129.16"]

}
resource "azurerm_subnet" "subnetDB" {
  name                 = "sn-dbsubnet-${var.project_name}-${var.location}"
  resource_group_name  = azurerm_resource_group.rg.name
  virtual_network_name = azurerm_virtual_network.vnet.name
  address_prefixes      = ["10.0.10.0/24"]
}
resource "azurerm_subnet" "subnetWEB" {
  name                 = "sn-websubnet-${var.project_name}-${var.location}"
  resource_group_name  = azurerm_resource_group.rg.name
  virtual_network_name = azurerm_virtual_network.vnet.name
  address_prefixes       = ["10.0.20.0/24"]
}
resource "azurerm_network_security_group" "allowWeb" {
  name                = "nsg-allowWeb-${var.project_name}-${var.location}"
  location            = azurerm_resource_group.rg.location
  resource_group_name = azurerm_resource_group.rg.name

  security_rule {
    name                       = "allowWeb"
    priority                   = 100
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "80"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }

  security_rule {
    name                       = "allowSSH"
    priority                   = 110
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "22"
    source_address_prefix      = "194.90.46.191"
    destination_address_prefix = "*"
  }
}

resource "azurerm_network_security_group" "denyWeb" {
  name                = "nsg-denyWeb-${var.project_name}-${var.location}"
  location            = azurerm_resource_group.rg.location
  resource_group_name = azurerm_resource_group.rg.name

    security_rule {
    name                       = "allowSSH"
    priority                   = 100
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "22"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }
  security_rule {
    name                       = "allowSQL"
    priority                   = 200
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "5432"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }
  security_rule{
    name                        = "allowOut"
    priority                    = 100
    direction                   = "Outbound"
    access                      = "Allow"
    protocol                    = "*"
    source_port_range           = "*"
    destination_port_range      = "*"
    source_address_prefix       = "*"
    destination_address_prefix  = "0.0.0.0/0"
    }
}
resource "azurerm_subnet_network_security_group_association" "denyWeb" {
  subnet_id                 = azurerm_subnet.subnetDB.id
  network_security_group_id = azurerm_network_security_group.denyWeb.id
}
resource "azurerm_subnet_network_security_group_association" "allowWeb" {
  subnet_id                 = azurerm_subnet.subnetWEB.id
  network_security_group_id = azurerm_network_security_group.allowWeb.id
}
resource   "azurerm_public_ip"   "webip"   { 
   name   =   "pip1" 
   location   =   var.location
   resource_group_name   =   azurerm_resource_group.rg.name 
   allocation_method   =   "Dynamic" 
   sku   =   "Basic" 
 } 
resource   "azurerm_network_interface"   "webnic"   { 
   name   =   "myvm1-nic" 
   location   =   var.location
   resource_group_name   =   azurerm_resource_group.rg.name 

   ip_configuration   { 
     name   =   "ipconfig1" 
     subnet_id   = azurerm_subnet.subnetWEB.id
     private_ip_address_allocation   =   "Dynamic" 
     public_ip_address_id   =   azurerm_public_ip.webip.id 
   } 
 }
resource   "azurerm_public_ip"   "dbip"   { 
   name   =   "dbip" 
   location   =   var.location
   resource_group_name   =   azurerm_resource_group.rg.name 
   allocation_method   =   "Dynamic" 
   sku   =   "Basic" 
 } 
resource   "azurerm_network_interface"   "dbnic"   { 
   name   =   "db-nic" 
   location   =   var.location
   resource_group_name   =   azurerm_resource_group.rg.name 

   ip_configuration   { 
     name   =   "dbipconfig" 
     subnet_id   = azurerm_subnet.subnetDB.id
     private_ip_address_allocation   =   "Dynamic" 
     public_ip_address_id   =   azurerm_public_ip.dbip.id 
   } 
 }
resource "tls_private_key" "linuxweb_key" {
  algorithm = "RSA"
  rsa_bits  = 4096
}
resource "tls_private_key" "linuxdb_key" {
  algorithm = "RSA"
  rsa_bits  = 4096
}
resource "local_file" "fileweb"{
  filename="C:\\Users\\ofekb\\.ssh\\web_final-proj.pem"
  content=tls_private_key.linuxweb_key.private_key_pem
}
resource "local_file" "filedb"{
  filename="C:\\Users\\ofekb\\.ssh\\db_final-proj.pem"
  content=tls_private_key.linuxdb_key.private_key_pem
}
resource "azurerm_storage_account" "sa-vm-db" {
  name = "sa-db"
  location = azurerm_resource_group.rg.location
  resource_group_name = azurerm_resource_group.rg.name
  account_tier             = "Standard"
  account_replication_type = "GRS"
}

resource "azurerm_managed_disk" "db_volume" {
  name= "db-volume"
  location = azurerm_resource_group.rg.location
  resource_group_name = azurerm_resource_group.rg.name
  storage_account_type = "Standard_LRS"
  create_option = "Empty"
  disk_size_gb = 4
  storage_account_id = azurerm_storage_account.sa-vm-db.id
}

resource "azurerm_linux_virtual_machine" "web-vm" {
  name                  = "vm-web-${var.project_name}-${var.location}"
  location              = var.location
  resource_group_name   = azurerm_resource_group.rg.name
  network_interface_ids = [azurerm_network_interface.webnic.id]
  size                  = "Standard_DS1_v2"

  os_disk {
    name                 = "WEBDisk"
    caching              = "ReadWrite"
    storage_account_type = "Standard_LRS"
  }

  source_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "18.04-LTS"
    version   = "latest"

  }

  computer_name                   = "web-vm"
  admin_username                  = "azureuser"
  disable_password_authentication = true

  admin_ssh_key {
    username   = "azureuser"
    public_key = tls_private_key.linuxweb_key.public_key_openssh
  }
    provisioner "file" {
    connection {
      type        = "ssh"
      host        = azurerm_linux_virtual_machine.web-vm.public_ip_address
      user        = "azureuser"
      private_key = tls_private_key.linuxweb_key.private_key_pem
    }
    source      = "C:/Users/ofekb/Desktop/working_terraform/terra_app.py"
    destination = "/tmp/app.py"  
  }
  provisioner "remote-exec" {
    connection {
      type        = "ssh"
      host        = azurerm_linux_virtual_machine.web-vm.public_ip_address
      user        = "azureuser"
      private_key = tls_private_key.linuxweb_key.private_key_pem
    }
    
inline = [
  "sudo apt-get update",
  "sudo apt-get install -y software-properties-common",
  "sudo add-apt-repository universe",  
  "sudo apt-get update",
  "sudo apt-get install -y python3-pip",
  "sudo python3 -m pip install --upgrade pip",
  "sudo apt-get install -y libpq-dev", 
  "sudo python3 -m pip install psycopg2-binary",
  "sudo python3 -m pip install flask",
  "export DB_PASSWORD=${var.db_password}",
  "sudo chmod +x /tmp/app.py",
  "sudo python3 /tmp/app.py &",

]
  }
}
locals {
  pg_hba_conf_content = file("C:/Users/ofekb/Desktop/working_terraform/pg_hba.conf")
}
resource "azurerm_linux_virtual_machine" "db-vm" {
  name                  = "vm-db-${var.project_name}-${var.location}"
  location              = var.location
  resource_group_name   = azurerm_resource_group.rg.name
  network_interface_ids = [azurerm_network_interface.dbnic.id]
  size                  = "Standard_DS1_v2"

  os_disk {
    name                 = "DBDisk"
    caching              = "ReadWrite"
    storage_account_type = "Standard_LRS"
  }

  source_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "18.04-LTS"
    version   = "latest"

  }

  computer_name                   = "db-vm"
  admin_username                  = "azureuser"
  disable_password_authentication = true

  admin_ssh_key {
    username   = "azureuser"
    public_key = tls_private_key.linuxdb_key.public_key_openssh
  }
  provisioner "file" {
    connection {
      type        = "ssh"
      host        = azurerm_linux_virtual_machine.db-vm.public_ip_address
      user        = "azureuser"
      private_key = tls_private_key.linuxdb_key.private_key_pem
    }
    source      = "C:/Users/ofekb/Desktop/test/pg_hba.conf"
    destination = "/tmp/pg_hba.conf"  
  }
  provisioner "remote-exec" {
    connection {
      type        = "ssh"
      host        = azurerm_linux_virtual_machine.db-vm.public_ip_address
      user        = "azureuser"
      private_key = tls_private_key.linuxdb_key.private_key_pem
    }
    
    inline = [
      "sudo parted /dev/sdc --script mklabel gpt mkpart xfspart xfs 0% 100%",
      "sudo mkfs.xfs /dev/sdc1",
      "sudo partprobe /dev/sdc1",
      "sudo mkdir /datadrive",
      "sudo mount /dev/sdc1 /datadrive",
      "sudo cd /datadrive",
      "sudo apt-get update",
      "sudo apt-get install -y postgresql",
      "sleep 55",
      "sudo -u postgres psql -c \"CREATE DATABASE wightloss;\"",
      "sudo -u postgres psql -c \"CREATE USER ofek WITH PASSWORD '${var.db_password}';\"",
      "sudo -u postgres psql -d wightloss -c \"CREATE TABLE weight_data (id SERIAL PRIMARY KEY, name VARCHAR(255), weight_value FLOAT, time TIMESTAMP);\"",
      "sudo mv /tmp/pg_hba.conf /etc/postgresql/10/main/pg_hba.conf",
      "sudo systemctl restart postgresql",
      "sudo -u postgres psql -d wightloss -c \"INSERT INTO weight_data (n55e, weight_value, time) VALUES ('Alice', 150.5, NOW());\"",
      "sudo -u postgres psql -d wightloss -c \"INSERT INTO weight_data (name, weight_value, time) VALUES ('Bob', 180.0, NOW());\"",
      "sudo -u postgres psql -d wightloss -c \"INSERT INTO weight_data (name, weight_value, time) VALUES ('Charlie', 165.8, NOW());\"",
      "sudo -u postgres psql -d wightloss -c \"INSERT INTO weight_data (name, weight_value, time) VALUES ('Diana', 130.2, NOW());\"",
      "sudo -u postgres psql -d wightloss -c \"INSERT INTO weight_data (name, weight_value, time) VALUES ('Eve', 155.7, NOW());\"",
      "sudo -u postgres psql -d wightloss -c \"GRANT SELECT ON TABLE weight_data TO ofek;\"",
    ]
  }
}
