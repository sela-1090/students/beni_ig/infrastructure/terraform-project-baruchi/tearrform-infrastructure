this is a terraform main.tf file that will create - a virtual network with two subnets, one contains a database and another contains a virtual machine that will act as a web server. the pg_hba.conf file is a postgres database configuration file.


To install the infrastructure, run the following commands in a terminal:

$ git clone https://gitlab.com/sela-1090/students/beni_ig/infrastructure/terraform-project-baruchi/tearrform-infrastructure  

$ cd tearrform-infrastructure


Open a PowerShell/CMD terminal in the terraform directory and run the following commands:

$ terraform init  



you can run terraform validate to validate the syntax of Terraform files


$ terraform plan

$ terraform apply 


Once you complete the application process, you will receive a public IP address as output. Please insert this IP address into the URL and press enter. Then, you can proceed to input your data. If you wish to view the data later, simply visit http://<public_ip_address>/data/ and press enter.
